using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskRestApi.Models;
using System.Linq;
using System.Collections;

namespace TaskRestApi.Controllers
{   
    [ApiController]
    [Route("api/[controller]")]
    public class StudentApiController
    {
        private static  List<StudentModel> data = new List<StudentModel>
        {
        new StudentModel{Id=1,Name="asim",Gender="male",Department="cs",DateOfBirth= DateTime.Now.Date},
         new StudentModel{Id=2,Name="wasim",Gender="male",Department="cs",DateOfBirth= DateTime.Now.Date}
        };
        //will retrun all data
        //api/studentapi
        [HttpGet("list")]
        public IEnumerable GetAll(){
           return data;
        }
        // will retrun single  data
        // api/studentapi/1
        [HttpGet("single/{id}")]
        public StudentModel GetSingle(int id){
           var rec = data.Single(x=>x.Id==id);
            return rec;       
        }
        // //will create  data
        // //api/studentapi
        [HttpPost("create")]
        public  void Post([FromBody] StudentModel item){
            data.Add(item);
            
        }
        // //will update single data
        // //api/studentapi/1
        [HttpPut("update/{id}")]
        public void Put([FromBody] StudentModel item,int Id){
            data[Id]=item;
        }
        // //will delete data
        // //api/studentapi/1
        [HttpDelete("delete/{id}")]
        public void Delete(int Id){
            data.RemoveAt(Id);
        }
    }
}