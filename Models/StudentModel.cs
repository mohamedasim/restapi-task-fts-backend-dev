using System;
namespace TaskRestApi.Models
{
    public class StudentModel
    {
        public int Id{get;set;}
        public string Name{get;set;}
        public string Gender{get;set;}
        public string Department{get;set;}
        public DateTime DateOfBirth{get;set;}
    }
}